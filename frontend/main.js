import Vue from 'vue';
import VueRouter from 'vue-router';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.css';
import 'font-awesome/css/font-awesome.css';
import 'highlight.js/styles/github.css';
import './styles.scss';
import auth from './auth';
import VueClip from 'vue-clip'
import VeeValidate from 'vee-validate';

Vue.use(VeeValidate);
Vue.use(VueClip);
Vue.use(VueRouter);

Vue.prototype.$http = axios;

Vue.component('app-footer', require('./components/app-footer.vue'));
Vue.component('app-header', require('./components/app-header.vue'));

const router = new VueRouter({
    mode: 'history',
    routes: [
        { path: '/', component: require('./components/hero.vue'), meta: { title: "Alexey Bondarenko"} },
        { path: '/blog', component: require('./components/blog.vue'), meta: { title: "Blog"} },
        { path: '/about', component: require('./components/about.vue'), meta: { title: "About"} },
        { path: '/contact', component:  require('./components/contact.vue'), meta: { title: "Contact"} },
        {
            path: '/blog/create',
            name: 'create',
            component: require('./components/create.vue'),
            meta: { title: "Create"},
            beforeEnter: (to, from, next) => {
                if (!auth.isAdmin) {
                    return next({path: '/'});
                }
                return next();
            }
        },
        { path: '/blog/:id', name: 'post', component: require('./components/post.vue') },
        { path: '*', component: require('./components/404.vue')}
    ]
});

router.beforeEach((to, from, next) => {
    if (typeof to.meta.title !== 'undefined'){
        document.title = to.meta.title
    }
    next()
});

new Vue({
    el: '#app',
    router: router,
    data: {
        loading: true
    },
    mounted: function () {
        let that = this;
        window.onload = function () {
            that.$root.loading = false;
        }
    }
});