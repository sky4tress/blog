const express = require('express');
const router = express.Router();


router.post('/', function(req, res, next) {
    if (!req.files) {
        return res.status(400).send('No files were uploaded.');
    }
    // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
    let file = req.files.file;
    // Use the mv() method to place the file somewhere on your server
    file.mv('./public/upload/' + file.name, function(err) {
        if (err) {
            return res.status(500).send(err);
        }
        res.status(201).send('File uploaded!');
    });
});

module.exports = router;
