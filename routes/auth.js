const express = require('express');
const router = express.Router();
const low = require('lowdb');
const db = low('db.json');

// Logout endpoint
router.get('/logout', function (req, res) {
    req.session.destroy();
    res.send("logout success!");
});

router.post('/login', function (req, res) {
     if(req.body.password == db.get('password')) {
        req.session.admin = true;
        return res.status(200).send();
    }
    res.status(401).json({error: 'wrong password'});
});

module.exports = router;
