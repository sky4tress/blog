const express = require('express');
const router = express.Router();
const nodemailer = require('nodemailer');

// create reusable transporter object using the default SMTP transport
let transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'sky4tress@gmail.com',
        pass: 'zcjbrpaemynvrzmb'
    }
});

router.post('/', function(req, res, next) {
    let mailOptions = {
        from: req.body.name + ' <' + req.body.email +'>',
        to: 'sky4tress@gmail.com',
        subject: '✔ New message via blog contact form',
        text: req.body.message
    };
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message %s sent: %s', info.messageId, info.response);
        res.status(201).send();
    });
});

module.exports = router;
