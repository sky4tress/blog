var express = require('express');
var router = express.Router();
const version = require("../package.json").version;
/* GET home page. */
router.get('/', function(req, res, next) {

    res.render('index', { title: 'Alexey Bondarenko', version: version });
});

module.exports = router;
