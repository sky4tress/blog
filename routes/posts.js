const express = require('express');
const low = require('lowdb');
const moment = require('moment');
const router = express.Router();
const db = low('db.json');

router.post('/', function (req, res, next) {
    if (!req.session.admin) {
        return res.status(403).send();
    }
    let post = req.body;
    post.url = post.title.toLowerCase().replace(new RegExp(' ', 'g'), '-');
    post.created = new Date();
    db.get('posts')
        .push(post)
        .write();
    res.status(201).send();
});

router.get('/', function (req, res, next) {
    let limit = parseInt(req.query.limit) || 5;
    let offset = parseInt(req.query.offset) || 0;
    let posts = db.get('posts')
        .cloneDeep()
        .map(function(item) {
            item.created = moment(item.created);
            return item;
        })
        .orderBy(['created'], ['desc'])
        .slice(offset, offset + limit)
        .map(function(item) {
            item.created = item.created.calendar();
            return item;
        });
    res.status(200).send(posts);
});

router.get('/:url', function (req, res, next) {
    let post = db.get('posts').find({url: req.params.url}).toJSON();
    if (!post) {
        res.status(404).send();
        return;
    }
    res.status(200).json(post);
});

module.exports = router;
