var express = require('express');
var router = express.Router();

var webpack = require("webpack");
var webpackConfig = require("../webpack.config");
var compiler = webpack(webpackConfig);

router.use(require("webpack-dev-middleware")(compiler, {
    noInfo: true, publicPath: webpackConfig.output.publicPath,
    watchOptions: {
        aggregateTimeout: 300,
        poll: 1000
    }
}));

router.use(require("webpack-hot-middleware")(compiler, {
    log: console.log, path: '/__webpack_hmr', heartbeat: 10 * 1000
}));


module.exports = router;
